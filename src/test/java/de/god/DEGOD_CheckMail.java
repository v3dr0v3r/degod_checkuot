package de.god;

import degod.ui.pages.SubscriptionPage;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DEGOD_CheckMail {

    private String Username;
    private String Password;

    private DEGOD_CheckMail() {
        Username = "somejackblackmail@gmail.com";
        Password = "s0m3p4ssw0rd";
    }


    @BeforeMethod(alwaysRun = true)
    protected void setUp(){
        System.out.println("Check Mail Starts");
    }

    @Test
    protected void DEGOD_Checkout_SampleTest() {

        new SubscriptionPage().checkGMail(Username, Password);

    }

    @AfterMethod(alwaysRun = true)
    private void teardown() {
        System.out.println("Check Mail Success");
    }

}
