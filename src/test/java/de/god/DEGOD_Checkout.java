package de.god;

import degod.ui.pages.HomePage;
import degod.ui.pages.SubscriptionPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import degod.ui.Browser;
import degod.ui.Locator;


public class DEGOD_Checkout  {

    private String baseURLStage;
    private String name;
    private String email;
    private String Description;
    private String Username;
    private String Password;

    private DEGOD_Checkout() {
        baseURLStage = "http://www.god.de/";
        name = "someuser";
        email = "somejackblackmail@gmail.com";
        Description = "This is my Selenium";
        Username = "somejackblackmail@gmail.com";
        Password = "s0m3p4ssw0rd";

    }


    @BeforeMethod(alwaysRun = true)
    protected void setUp() throws Exception {
          new Locator().generateObjectHash("/src/main/java/degod/ui/elements/elementsRepo.txt");
    }

    @Test
    protected void DEGOD_Checkout_SampleTest() throws Exception {

                                   new Browser().openBrowser(baseURLStage);
                                   new HomePage().clickKontaktButton();
        SubscriptionPage subpage = new SubscriptionPage();
                         subpage.enterNameField(name);
                         subpage.enterEmailField(email);
                         subpage.enterDescriptionField(Description);
                         subpage.clickSendenButton();
                         subpage.checkGMail(Username, Password);

    }

    @AfterMethod(alwaysRun = true)
    private void teardown() {
          new Browser().closeBrowser();
    }

}
