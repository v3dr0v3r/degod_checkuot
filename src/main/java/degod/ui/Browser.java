package degod.ui;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.codeborne.selenide.Configuration;

import static degod.ui.GlobalVariables.defaultBrowser;
import static degod.ui.GlobalVariables.defaultEnvironment;


public class Browser {

    protected static WebDriver driver;
    protected static WebDriverWait wait;

    public void openBrowser(String url) {
        new Browser();
        String browserToUse = defaultBrowser;
        String environmentToUse = defaultEnvironment;

        System.out.println("Entering switch with: " + browserToUse);

        //* Supported values: "chrome", "firefox", "ie", "htmlunit", "phantomjs", "opera"
        switch (browserToUse) {

            case "chrome":
                Configuration.browser = "chrome";
                Configuration.pageLoadStrategy = "normal";
                ChromeOptions chromeOptions = new ChromeOptions();


                chromeOptions.addArguments("--disable-plugins");
                chromeOptions.addArguments("--disable-bundled-ppapi-flash");
                chromeOptions.addArguments("--disable-internal-flash");
                chromeOptions.addArguments("--disable-plugins-discovery");
                chromeOptions.addArguments("--start-maximized");
                chromeOptions.addArguments("--disable-popup-blocking");
                chromeOptions.addArguments("--disable-infobars");
                chromeOptions.addArguments("--disable-device-discovery-notifications");
                chromeOptions.addArguments("--incognito");


                driver = new ChromeDriver(chromeOptions);
                driver.manage().deleteAllCookies();
                driver.manage().window().fullscreen();


                ExtentReports report = new ExtentReports();
                String filePath = System.getProperty("user.dir") + "/test-output/report.html";
                ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(filePath);
                report.attachReporter(htmlReporter);
                report.setSystemInfo("ENV", "staging");
                report.setSystemInfo("browser", defaultBrowser);
                report.setSystemInfo("OS", System.getProperty("os.name"));
                htmlReporter.config().setReportName("m4k5 Automated Test Reports");

                break;
            case "firefox":
                Configuration.browser = "firefox";
                FirefoxOptions firefoxOptions = new FirefoxOptions();

                firefoxOptions.addArguments("--disable-plugins");
                firefoxOptions.addArguments("--start-maximized");

                driver = new FirefoxDriver(firefoxOptions);
                break;
            case "ie":
                Configuration.browser = "ie";
                InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();

                driver = new InternetExplorerDriver(internetExplorerOptions);
                break;

            default:
                System.out.println("Unknown browser!");
        }

        int defaultWaitTime = 20;
        wait = new WebDriverWait(driver, defaultWaitTime);
        System.out.println("Env is " + environmentToUse);

        driver.get(url);
        defaultBrowser = browserToUse;
        System.out.println("Opened " + url + " with " + defaultBrowser);


    }

    public void closeBrowser() {
        new Browser();
        driver.quit();
        System.out.println("Closed browser window");
    }
}
