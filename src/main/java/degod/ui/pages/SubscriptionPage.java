package degod.ui.pages;

import degod.ui.BasePage;
import degod.ui.Locator;


public class SubscriptionPage extends BasePage{
    private Locator locator = new Locator();

    public void enterNameField(String text) {

        enterText(locator.lookupClickableElement("inputNameField"), text);
    }

    public void enterEmailField(String text) {
        enterText(locator.lookupClickableElement("inputEmailField"), text);
    }

    public void enterDescriptionField(String text) {
        enterText(locator.lookupClickableElement("inputDescriptionField"), text);
    }

    public void clickSendenButton() throws InterruptedException {
        click(locator.lookupClickableElement("sendenButton"));
        Thread.sleep(1000);
    }

    public void checkGMail(final String Username, final String Password){
        checkMail(Username, Password);

    }

}
