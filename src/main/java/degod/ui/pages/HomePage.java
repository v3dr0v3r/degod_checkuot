package degod.ui.pages;

import degod.ui.BasePage;
import degod.ui.Locator;


public class HomePage extends BasePage {

     public void clickKontaktButton() {
        click(new Locator().lookupClickableElement("kontaktButton"));
    }
}

