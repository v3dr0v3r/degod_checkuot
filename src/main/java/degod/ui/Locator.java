package degod.ui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;


import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class Locator {

    private static HashMap<String, String> map = new HashMap<>();

    // Need to read text file and build up hash map then create the element based on the hashed value
    public void generateObjectHash(String repoTextFile) throws IOException {
        new Locator();
        String userDirectory = System.getProperty("user.dir");
        BufferedReader br = new BufferedReader(new FileReader(userDirectory + repoTextFile));
        String line;
        while (null != (line = br.readLine())) {
            String arr[] = line.split("\t");
            if (arr.length == 1) {
                if (arr[0].length() == 0 || arr[0].contains("***")) {
                    System.out.println("The line of - " + arr[0] + "- has a key and a value.");
                } else {
                    System.out.println("The line of - " + arr[0] + "- does not have a key and a value. Please update this object before continuing");
                }
            } else {
                map.put(arr[0], arr[1]);
            }
        }
        br.close();
    }

    public WebElement lookupClickableElement(String objectLookupKey) {

        String myPath = map.get(objectLookupKey);
        By byLocator;
        System.out.println("Attempting to find the object in repository with key " + objectLookupKey + " and value " + myPath);

        if (myPath.contains("//")) {
            byLocator = By.xpath(myPath);
        } else if (myPath.contains("id=")) {
            byLocator = By.id(myPath.split("id=")[1]);
        } else if (myPath.contains("css=")) {
            byLocator = By.cssSelector(myPath.split("css=")[1]);
        } else if (myPath.contains("className=")) {
            byLocator = By.className(myPath.split("className=")[1]);
        } else if (myPath.contains("name=")) {
            byLocator = By.name(myPath.split("name=")[1]);
        } else if (myPath.contains("partialLinkText=")) {
            byLocator = By.partialLinkText(myPath.split("partialLinkText=")[1]);
        } else if (myPath.contains(".")) {
            byLocator = By.cssSelector(myPath.split("css=")[1]);
        } else if (myPath.contains("href=")) {
            byLocator = By.cssSelector(myPath.split("css=")[1]);
        } else if (myPath.contains("(//")) {
            byLocator = By.xpath(myPath);
        } else if (myPath.contains("a")) {
            byLocator = By.cssSelector(myPath.split("css=")[1]);
        } else {
            System.out.println("Identifier " + objectLookupKey + " has a value of " + myPath + " that doesn't match any known format. Attempting to continue as an id.");
            byLocator = By.id(myPath);
        }


        Browser.wait.until(ExpectedConditions.elementToBeClickable(byLocator));
        return Browser.driver.findElement(byLocator);
    }

}
