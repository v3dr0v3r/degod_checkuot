package degod.ui;

import org.openqa.selenium.WebElement;

import javax.mail.*;
import java.io.IOException;
import java.util.Properties;

import static org.testng.AssertJUnit.fail;


public class BasePage {


    // Generic action to click on a field. Typically used with buttons, links, checkboxes, but can be used by anything
    protected void click(WebElement buttonToClick) {
        if (buttonToClick == null) {
            System.out.println("FAILS ------ The element does not exist.");
            fail("FAILS ------ The element has not been found.");
        } else {
            buttonToClick.click();
        }
    }

    // Generic action to enter text into a field. Typically used with text fields
    protected void enterText(WebElement elementToSet, String valueToSet) {
        System.out.println("Entering " + valueToSet + " into WebElement " + elementToSet.getTagName());
        elementToSet.clear();
        elementToSet.click();
        elementToSet.sendKeys(valueToSet);
    }
    // Generic action to check Gmail postbox
    protected void checkMail(final String Username, final String Password) {
        Folder folder = null;
        Store store = null;
        System.out.println("***Reading mailbox...");
        try {
            Properties props = new Properties();
            props.put("mail.store.protocol", "imaps");
            Session session = Session.getInstance(props);
            store = session.getStore("imaps");
            store.connect("imap.gmail.com", Username, Password);
            folder = store.getFolder("INBOX");
            folder.open(Folder.READ_ONLY);
            Message[] messages = folder.getMessages();
            System.out.println("No of Messages : " + folder.getMessageCount());
            System.out.println("No of Unread Messages : " + folder.getUnreadMessageCount());
            for (int i=0; i < messages.length; i++) {
                System.out.println("Reading MESSAGE # " + (i + 1) + "...");
                Message msg = messages[i];
                String strMailSubject, strMailBody;

                Object subject = msg.getSubject();
                Object content = msg.getContent().toString();

                strMailSubject = (String)subject;
                strMailBody = (String)content;

                System.out.println(strMailSubject);
                System.out.println(strMailBody);

            }
        }catch(MessagingException | IOException messagingException){
            messagingException.printStackTrace();
        } finally {
            if (folder != null) {
                try {
                    folder.close(true);
                } catch (MessagingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (store != null) {
                try {
                    store.close();
                } catch (MessagingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
}

